<?php

require __DIR__ . '/vendor/autoload.php';

array_shift($argv);

\App\Main::main($argv);
